Categories:Theming
License:MIT
Web Site:http://alexanderfedora.blogspot.com
Source Code:https://github.com/ghisguth/sunlight/tree/master/blurredlines
Issue Tracker:https://github.com/ghisguth/sunlight/issues

Name:Blurred Lines
Auto Name:Blurred Lines Live Wallpaper
Summary:Live wallpaper
Description:
This is small live wallpaper inspired by 1K demo [http://www.tylerdurden.net.ru
"In the mist of web"]. Website is in Russian.
.

Repo Type:git
Repo:https://github.com/ghisguth/sunlight.git

#old repo
Build:1.1-2,9
    commit=4310949

Build:1.2,10
    commit=9457a7ebd586b6aa00159fd1c7ac11852a88c38a
    subdir=blurredlines

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2
Current Version Code:10

