Categories:Multimedia,Internet
License:GPLv3
Web Site:https://github.com/de-live-gdev/cherrymusic_android/blob/HEAD/README.md
Source Code:https://github.com/de-live-gdev/cherrymusic_android
Issue Tracker:https://github.com/de-live-gdev/cherrymusic_android/issues

Auto Name:Cherry
Summary:Wrapper for CherryMusic
Description:
Wrapper for CherryMusic, a self-hosted streaming web-application. A lightweight
alternative to Ampache, Google Play Music,Spotify,..

See [http://www.fomori.org/cherrymusic/] for more informations.
.

Repo Type:git
Repo:https://github.com/de-live-gdev/cherrymusic_android.git

Build:1.0,1
    commit=v1.0

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1

